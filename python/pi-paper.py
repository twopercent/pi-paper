#!/usr/bin/python3

import sys
import os
import time
import configparser
import requests
from pycoingecko import CoinGeckoAPI
from datetime import date
from datetime import datetime

picdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'pic')
libdir = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'lib')
if os.path.exists(libdir):
    sys.path.append(libdir)

from waveshare_epd import epd2in7
from PIL import Image, ImageDraw, ImageFont

### CEL Price
cg = CoinGeckoAPI()
celdict = cg.get_price(ids='celsius-degree-token', vs_currencies='usd')
celusd = str(celdict["celsius-degree-token"]["usd"])

btcdict = cg.get_price(ids='bitcoin', vs_currencies='usd')
btcusd = str(btcdict["bitcoin"]["usd"])

ethdict = cg.get_price(ids='ethereum', vs_currencies='usd')
ethusd = str(ethdict["ethereum"]["usd"])

### Date and Time
day = str(date.today())
now = datetime.now()
time = str(now.strftime("%H:%M:%S"))

### Gas price
config = configparser.ConfigParser()
config.read('pi-paper.config')
module = config.get('etherscan', 'module')
action = config.get('etherscan', 'action')
apikey = config.get('etherscan', 'apikey')

payload = {'module': module, 'action': action, 'apikey': apikey}
r = requests.get('https://api.etherscan.io/api', params=payload)
gas_price = r.json()["result"]["ProposeGasPrice"]


try:
    # Display init, clear
    display = epd2in7.EPD()
#   display.init(display.lut_full_update)
#   display.init(display.lut_partial_update)
   
    display.init()
    display.Clear(0) # 0: Black, 255: White
    w = display.height
    h = display.width
#   print('width:', w)
#   print('height:', h)
    ### ... IMAGE CODE ... ###

    body = ImageFont.truetype(os.path.join(picdir, 'Font.ttc'), 18)
    
    image = Image.new(mode='1', size=(w, h), color=255)
    draw = ImageDraw.Draw(image)
    draw.text((0, 0), day + "    " + time, font=body, fill=0, align='left')
    draw.text((0, 28), 'BTC $' + btcusd, font=body, fill=0, align='left')
    draw.text((0, 46), 'ETH $' + ethusd, font=body, fill=0, align='left')
    draw.text((0, 64), 'CEL $' + celusd, font=body, fill=0, align='left')
    draw.text((0, 92), 'Gas Price ' + gas_price + ' Gwei', font=body, fill=0, align='left')
    display.display(display.getbuffer(image))



#    time.sleep(5)
    
#    draw.text((0, 0), 'Quoth the Raven "Somemore."', font=body, fill=0, align='left')
#    display.display(display.getbuffer(image))

except IOError as e:
    print(e)
