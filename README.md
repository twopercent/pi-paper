## Waveshare 2.7inch e-Paper HAT

### Setup
Check the Hardware/Software setup section of the [Waveshare](https://www.waveshare.com/wiki/2.7inch_e-Paper_HAT) page to install all the necessary dependancies.

Add pycoingecko python package with `sudo pip3 install pycoingecko`

Acquire an API key from [etherscan](https://etherscan.io/apis)

Rename `pi-paper.config.example` to `pi-paper.config` and enter your API key

Copy or symlink `pi-paper_cron` to `/etc/cron.hourly`
